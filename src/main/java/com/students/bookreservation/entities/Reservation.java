package com.students.bookreservation.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "book_reader")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "book_id", referencedColumnName = "id")
    @OneToOne
    private Book book;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne
    private User user;

    @Column(name = "time_of_reservation")
    private Long timeReservation;


    public Reservation(Book book, User user) {
        this.book = book;
        this.user = user;
        this.timeReservation =  System.currentTimeMillis();
    }
}
