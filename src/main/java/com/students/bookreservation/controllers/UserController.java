package com.students.bookreservation.controllers;

import com.students.bookreservation.dto.UserDTO;
import com.students.bookreservation.entities.User;
import com.students.bookreservation.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable("id") Integer id) {
        final User user = userService.find(id);
        if (user != null) {
            return ResponseEntity.ok(new UserDTO(user.getId(), user.getEmail(), user.getPassword()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO userDTO) {
        final User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        userService.save(user);
        return ResponseEntity.ok().build();
    }
}
