package com.students.bookreservation.repositories;

import com.students.bookreservation.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class UserRepositoryImpl implements UserRepository  {

    @Autowired
    private EntityManager entityManager;


    @Override
    public User find(Integer id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void save(User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.flush();
        entityManager.getTransaction().commit();

    }

    @Override
    public void update(User user) {
        entityManager.merge(user);
    }
}
