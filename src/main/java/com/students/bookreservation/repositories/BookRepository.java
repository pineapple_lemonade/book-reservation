package com.students.bookreservation.repositories;

import com.students.bookreservation.dto.BookDTO;
import com.students.bookreservation.entities.Book;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository  {

    Book find(Integer id);

    List<BookDTO> findAll();

    void save(Book book);

    void update(Book book);
}
