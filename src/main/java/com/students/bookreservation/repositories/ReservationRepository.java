package com.students.bookreservation.repositories;

import com.students.bookreservation.dto.ReservationDTO;
import com.students.bookreservation.entities.Reservation;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository {

    Reservation find(Integer book_id);

    void save(Reservation reservation);

    void update(Reservation reservation);

    void delete(Integer book_id);

    List<Reservation> findAll();

}
