package com.students.bookreservation.dto;

import com.students.bookreservation.entities.Book;
import com.students.bookreservation.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Data
@NoArgsConstructor
public class ReservationDTO {

    private Book book;
    private User user;
    private Long timeReservation;



    public ReservationDTO(Book book, User user) {
        this.book = book;
        this.user = user;
        this.timeReservation = System.currentTimeMillis();
    }



}
