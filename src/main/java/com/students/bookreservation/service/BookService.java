package com.students.bookreservation.service;

import com.students.bookreservation.dto.BookDTO;
import com.students.bookreservation.entities.Book;

import java.util.List;

public interface BookService {

    Book find(Integer id);

    List<BookDTO> findAll();

    void save(Book book);

    void update(Book book);
}
