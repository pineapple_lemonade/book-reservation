package com.students.bookreservation.service;

import com.students.bookreservation.dto.BookDTO;
import com.students.bookreservation.entities.Book;
import com.students.bookreservation.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book find(Integer id) {
        return bookRepository.find(id);
    }

    @Override
    public List<BookDTO> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void update(Book book) {
        bookRepository.update(book);
    }
}
