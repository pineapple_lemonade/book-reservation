package com.students.bookreservation.service;

import com.students.bookreservation.entities.Reservation;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

@EnableScheduling
public class ReservationDeleteService {

    private final ReservationService reservationService;

    public ReservationDeleteService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Scheduled(fixedDelay = 86400000)
    public void removeOutdatedBook(){
        List<Reservation> all = reservationService.findAll();
        for (Reservation reservation : all) {
            if (System.currentTimeMillis() - reservation.getTimeReservation() > 604800000) {
                reservationService.delete(reservation.getBook().getId());
            }
        }
    }
}
